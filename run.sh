SCRIPT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

HELP_TEXT="Advent of Code - Bash

Usage: ./run.sh -y 2022 -d 1 -p 1 -t

Options:
    -h, --help           Displays this message and exits.
    -y, --year           The year of AoC to solve.
    -d, --day            The day of AoC to solve.
    -p, --part           The part of AoC to solve.
    -e, --example        Solves the example puzzle.
    -t, --timed          Times the script execution."

[[ $# -eq 0 ]] && echo "$HELP_TEXT" && exit 0
[[ "$1" =~ ^(-h|help|--help)$ ]] && echo "$HELP_TEXT" && exit 0

while (( "$#" )); do
    case "$1" in
        -y|--year) YEAR=$2; shift 2;;
        -d|--day) DAY=$2; shift 2;;
        -p|--part) PART=$2; shift 2;;
        -e|--example) EXAMPLE=1; shift;;
        -t|--timed) TIMED=1; shift;;
        *) echo "fatal: unknown argument '$1'"; exit 1;;
    esac
done

[[ -z "$YEAR" ]] && echo "fatal: 'year' is required" && exit 1
[[ -z "$DAY" ]] && echo "fatal: 'day' is required" && exit 1
[[ -z "$PART" ]] && echo "fatal: 'part' is required" && exit 1
[[ "$EXAMPLE" -eq 1 ]] && PARAM="-e" || PARAM=""
DAY=$(printf '%02d' $DAY)

DAY_PATH="${SCRIPT_PATH}/${YEAR}/day${DAY}/"
SOLVE="${SCRIPT_PATH}/${YEAR}/day${DAY}/solve-pt${PART}.sh"

! [[ -d "$DAY_PATH" ]] && echo "fatal: day path not found" && exit 1
! [[ -f "$SOLVE" ]] && echo "fatal: solve path not found" && exit 1

cd "$DAY_PATH"

if [[ "$TIMED" -eq 1 ]]; then
    TIMEFORMAT="%E"; time "$SOLVE" "$PARAM"
else
    "$SOLVE" "$PARAM"
fi

cd "$SCRIPT_PATH"
