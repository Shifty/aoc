#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

declare -A copies=()

for line in "${lines[@]}"; do
    game_id="${line%:*}"; game_id="${game_id#* }"
    copy_num=${copies[$((game_id))]:=1}
    matches=($(echo "$line " | grep -oP "(?<=\h)(\d+)(?=\h)(?=[\d\h]*\|[\d\h]*(\D)\1(\D|$))"))
    if [[ ${#matches[@]} -gt 0 ]]; then
        for n in $(eval "echo {1..${#matches[@]}}"); do
            orig=${copies[$((game_id+n))]:-1}
            copies[$((game_id+n))]=$((orig+copy_num))
        done
    fi
done

echo $(echo "${copies[@]// /+}" | tr ' ' '+' | bc)
