#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

sum=0

for line in "${lines[@]}"; do
    matches=$(echo "$line " | tr -s ' ' | grep -oP "(?<=\h)(\d+)(?=\h)(?=[\d\h]*\|[\d\h]*(\D)\1(\D|$))")
    matches=$(echo "$matches" | sed -re 's/[0-9]+/2/g' | paste -sd*)
    [[ -n "$matches" ]] && ((sum+=$(echo "$matches / 2" | bc)))
done

echo $sum
