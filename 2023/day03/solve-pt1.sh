#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

limit=$((${#lines[@]}))
sum=0

for ((i=0;i<=limit;i++)); do
    line="${lines[$i]}"
    while [[ "$line" =~ ^([^0-9]*)([0-9]+) ]]; do
        number="${BASH_REMATCH[2]}"
        offset=$((${#BASH_REMATCH[1]}-1))
        offset=$((offset<0?0:offset))
        length=$((${#BASH_REMATCH[2]}+((offset==0?1:2))))
        [[ $i -gt 0 ]] && chunk_a=${lines[$((i-1))]:$offset:$length}
        chunk_b=${lines[$((i))]:$offset:$length}
        [[ $i -lt $limit ]] && chunk_c=${lines[$((i+1))]:$offset:$length}
        echo "$chunk_a$chunk_b$chunk_c" | grep -oP "[^0-9.]" &>/dev/null && ((sum+=number))
        repl=$(printf -- '.%.0s' $(eval echo {1..${#number}}))
        line=$(echo "$line" | sed "0,/$number/{s/$number/$repl/}")
    done
done

echo $sum
