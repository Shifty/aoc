#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

limit=$((${#lines[@]}))
parts=()
sum=0

check() {
    chunk="$1"
    while [[ "$chunk" =~ ([.\*]*)([0-9]+) ]]; do
        num_str="${BASH_REMATCH[1]}"
        num_val="${BASH_REMATCH[2]}"
        case ${#num_val} in
            3) parts+=("$num_val");;
            2) [[ ${#num_str} -gt 0 && ${#num_str} -lt 5 ]] && parts+=("$num_val");;
            1) [[ ${#num_str} -gt 1 && ${#num_str} -lt 5 ]] && parts+=("$num_val");;
        esac
        repl=$(printf -- '.%.0s' $(eval echo {1..${#num_val}}))
        chunk=$(echo "$chunk" | sed "s/$num_val/$repl/")
    done
}

for ((i=0;i<=limit;i++)); do
    line=$(echo "${lines[$i]}" | sed -re 's/[^*.0-9]/./g')
    while [[ "$line" =~ ^([^*]*)\* ]]; do
        offset=${#BASH_REMATCH[1]}
        mod=$((offset==0?3: offset==1?4: offset==2?5:6))
        offset=$((offset-=3, offset<0?0:offset))
        length=$((1+mod))
        line=$(echo "$line" | sed "0,/\*/{s/\*/./}")
        check $(echo "${lines[$((i-1))]:$offset:$length}" | sed -re 's/[^*.0-9]/./g')
        check $(echo "${lines[$((i))]:$offset:$length}" | sed -re 's/[^*.0-9]/./g')
        check $(echo "${lines[$((i+1))]:$offset:$length}" | sed -re 's/[^*.0-9]/./g')
        [[ ${#parts[@]} -eq 2 ]] && ((sum+=$(echo "${parts[@]}" | grep -oP "\d+" | paste -sd* | bc)))
        unset parts
    done
done

echo $sum
