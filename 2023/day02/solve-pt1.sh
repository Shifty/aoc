#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

declare -A cubes=(
    ["red"]="12"
    ["green"]="13"
    ["blue"]="14"
)

sum=0

for line in "${lines[@]}"; do
    game_id="${line%:*}"
    game_id="${game_id#* }"
    while read -r cube; do
        color="${cube#* }"
        limit=${cubes[$color]}
        if [[ "${cube% *}" -gt $limit ]]; then
            continue 2
        fi
    done < <(echo "${line#*:}" | grep -oP "\d+ \w+")
    ((sum+=game_id))
done

echo $sum
