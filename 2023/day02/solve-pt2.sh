#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

sum=0

for line in "${lines[@]}"; do
    game_id="${line%:*}"
    game_id="${game_id#* }"
    while read -r cube; do
        value="${cube% *}"
        color="${cube#* }"
        if [[ $value -gt ${!color} ]]; then
            eval $color=$value
        fi
    done < <(echo "${line#*:}" | grep -oP "\d+ \w+")
    ((sum+=(red*green*blue)))
    unset red green blue
done

echo $sum
