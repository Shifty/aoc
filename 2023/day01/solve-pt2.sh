#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example-pt2.txt" || echo "puzzle.txt")

readarray -t lines < $infile

declare -A submap=(
    ["one"]="1"
    ["two"]="2"
    ["three"]="3"
    ["four"]="4"
    ["five"]="5"
    ["six"]="6"
    ["seven"]="7"
    ["eight"]="8"
    ["nine"]="9"
)
pattern=$(echo "${!submap[@]} ${submap[@]}" | tr ' ' '|')

sum=0
for line in "${lines[@]}"; do
    first=$(echo "$line" | grep -oE "$pattern" | head -1)
    first=${submap[$first]:-$first}
    last=$(echo "$line" | rev | grep -oE $(echo "$pattern" | rev) | head -1 | rev)
    last=${submap[$last]:-$last}
    ((sum+=$first$last))
done

echo $sum
