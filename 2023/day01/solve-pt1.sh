#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example-pt1.txt" || echo "puzzle.txt")

readarray -t lines < <(sed 's/[a-z]//g' $infile)

sum=0
for line in "${lines[@]}"; do
    ((sum+="${line:0:1}${line: -1}"))
done

echo $sum
