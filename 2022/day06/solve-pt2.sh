#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

line=$(cat "$infile")
last=""

unique() {
    for ((x=0; x<${#1}; x++)); do
        r="${1:$x:1}"
        r="${1//$r}"
        [[ ${#r} -lt 13 ]] && return 1
    done
    return 0
}

proc=0
for ((i=0; i<${#line}; i++)); do
    ((proc++))
    c="${line:$i:1}"
    last="$last$c"
    if [[ ${#last} -ge 14 ]]; then
        last="${last:(-14):14}"
        unique "$last"
        [[ $? -eq 0 ]] && echo $proc && break
    fi
done
