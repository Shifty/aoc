#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -td ';' groups < <(cat "$infile" | tr "\n" " " | sed 's/\s\s/;/g')

sums=()
for n in "${groups[@]}"; do
    sums+=($(("${n// /+}")))
done

echo "${sums[*]}" | tr " " "\n" | sort -n | tail -3 | paste -sd+ | bc
