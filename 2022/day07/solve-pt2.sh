#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"
cur="root"

declare -A sizes=(
    ["root"]=0
)
declare -A dirs=(
    ["root"]=""
)

addsize() {
    local part="$1"
    cs="$2"
    while true; do
        ss=$((${sizes["$part"]} + $cs))
        sizes["$part"]=$ss
        [[ "$part" == "root" ]] && break
        part="${part%/*}"
    done
}

for n in "${lines[@]:1}"; do
    if [[ "$n" =~ dir.* ]]; then
        dirs["$cur/${n##* }"]=""
        sizes["$cur/${n##* }"]=0
    elif [[ "$n" =~ [0-9].* ]]; then
        addsize "$cur" "${n%% *}"
    elif [[ "$n" =~ .*cd.* ]]; then
        if [[ "${n##* }" == ".." ]]; then
            cur="${cur%/*}"
        else
            cur="$cur/${n##* }"
            sizes["$cur"]=0
        fi
    fi
done

total=70000000
required=30000000
available=$(($total - ${sizes["root"]}))
needed=$(($required - $available))

sorted=($(printf '%s\n' "${sizes[@]}" | sort -n))

for size in "${sorted[@]}"; do
    if [[ $size -ge $needed ]]; then
        echo $size
        break
    fi
done
