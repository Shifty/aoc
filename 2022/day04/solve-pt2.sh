#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t pairs < "$infile"
count=0

for n in "${pairs[@]}"; do
    a=${n%%,*}
    as=${a%-*}
    ae=${a#*-}

    b=${n##*,}
    bs=${b%-*}
    be=${b#*-}

    if [[ ($as -le $bs && $be -le $ae) ]]; then
        ((count++))
    elif [[ ($bs -le $as && $ae -le $be) ]]; then
        ((count++))
    elif [[ ($as -le $bs && $ae -ge $bs) ]]; then
        ((count++))
    elif [[ ($as -le $be && $ae -ge $be) ]]; then
        ((count++))
    elif [[ ($bs -le $as && $be -ge $as) ]]; then
        ((count++))
    elif [[ ($bs -le $ae && $be -ge $ae) ]]; then
        ((count++))
    fi
done

echo $count
