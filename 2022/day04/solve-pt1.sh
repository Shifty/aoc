#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t pairs < "$infile"
count=0

for n in "${pairs[@]}"; do
    a=${n%%,*}
    as=${a%-*}
    ae=${a#*-}

    b=${n##*,}
    bs=${b%-*}
    be=${b#*-}

    if [[ $as -le $bs && $ae -ge $be ]]; then
        ((count++))
    elif [[ $bs -le $as && $be -ge $ae ]]; then
        ((count++))
    fi
done

echo $count
