#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"
crates=()

num=$(($(cat "$infile" | grep -nP "^ \d.+" | cut -d ':' -f1) - 1))
readarray -t l < <(tac "$infile" | tail -$num)
pattern="(?:(?<=^)|(?<= ))   ($| )"

for n in "${l[@]}"; do
    i=0
    n=$(echo "$n" | perl -pe "s/$pattern/[-] /g" | tr -d "[ ]")
    for (( c=0; c<${#n}; c++ )); do
        if [[ "${n:$i:1}" =~ [a-Z] ]]; then
            crates[$i]+="${n:$i:1}"
        fi
        ((i++))
    done
done

for n in "${lines[@]}"; do
    if [[ $n =~ "move" ]]; then
        m="${n//[!0-9 ]/}"
        m="${m#* }"
        qt="${m%% *}"
        _fr="${m% *}"
        fr="${_fr#* }"
        to="${m##* }"
        fr_c="${crates[$((fr - 1))]}"
        to_c="${crates[$(($to - 1))]}"
        for ((i=0; i<$qt; i++)); do
            c="${fr_c:(-1):1}"
            fr_c="${fr_c::-1}"
            to_c+="$c"
        done
        crates[$((fr - 1))]="$fr_c"
        crates[$((to - 1))]="$to_c"
    fi
done

ans=""
for c in "${crates[@]}"; do
    ans="${ans}${c:(-1):1}"
done

echo "$ans"
