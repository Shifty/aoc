#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")
bound=$([[ "$1" == "-e" ]] && echo "20" || echo "4000000")

readarray -t lines < "$infile"
declare -A pairs=()
declare -A yranges=()

lx=0
hx=0
ly=0
hy=0

for n in "${lines[@]}"; do
    [[ "${n%:*}" =~ x=-?[0-9]+ ]] && sx="${BASH_REMATCH[0]#*=}"
    [[ "${n%:*}" =~ y=-?[0-9]+ ]] && sy="${BASH_REMATCH[0]#*=}"
    [[ "${n#*:}" =~ x=-?[0-9]+ ]] && bx="${BASH_REMATCH[0]#*=}"
    [[ "${n#*:}" =~ y=-?[0-9]+ ]] && by="${BASH_REMATCH[0]#*=}"
    pairs["$sx,$sy"]="$bx,$by"
    plots+=("$sx,$sy")
    if [[ $sx -gt $hx ]]; then
        hx=$sx
    elif [[ $sx -lt $lx ]]; then
        lx=$sx
    elif [[ $sy -gt $hy ]]; then
        hy=$sy
    elif [[ $sy -lt $ly ]]; then
        ly=$sy
    fi
    if [[ $bx -gt $hx ]]; then
        hx=$bx
    elif [[ $bx -lt $lx ]]; then
        lx=$bx
    elif [[ $by -gt $hy ]]; then
        hy=$by
    elif [[ $by -lt $ly ]]; then
        ly=$by
    fi
done

for s in "${!pairs[@]}"; do
    b="${pairs[$s]}"
    dx=$((${s%,*} - ${b%,*}))
    dy=$((${s#*,} - ${b#*,}))
    d1=$((${dx#-} + ${dy#-}))
    ys=$((${s#*,} - ${d1#-}))
    ye=$((${s#*,} + ${d1#-}))
    pairs["$s"]=$d1
    yranges["$s"]="$ys,$ye"
done

sortrange() {
    flag=0
    for ((i=0; i<${#ranges[@]}; i++)); do
        for((j=0; j<${#ranges[@]}-i-1; j++)); do
            if [ ${ranges[j]%,*} -gt ${ranges[$((j+1))]%,*} ]; then
                temp=${ranges[j]}
                ranges[$j]=${ranges[$((j+1))]}
                ranges[$((j+1))]=$temp
                flag=1
            fi
        done
        if [[ $flag -eq 0 ]]; then
            break
        fi
    done
}

search() {
    local y="$1"
    local -a ranges=()
    for s in "${!pairs[@]}"; do
        syr="${yranges[$s]}"
        if [[ $y -lt ${syr%,*} || $y -gt ${syr#*,} ]]; then
            continue
        fi
        d1="${pairs[$s]}"
        d2=$((${s#*,} - y))
        xr=$((${d1#-} - ${d2#-}))
        xs=$((${s%,*} - ${xr#-}))
        xe=$((${s%,*} + ${xr#-}))
        xs=$((xs >= 0 ? xs : 0))
        xe=$((xe <= bound ? xe : bound))
        ranges+=("$xs,$xe")
    done

    sortrange
    local last

    for r in "${ranges[@]}"; do
        rs=${r%,*}
        re=${r#*,}
        if [[ $i -eq 0 ]]; then
            last=$re
            continue
        fi
        if [[ $rs -le $last && $re -gt $last ]]; then
            last=$re
        elif [[ $rs -gt $last ]]; then
            echo "$((last + 1)),$y"
            exit
        fi
    done
    ## log progress as this takes a very long time to run
    [[ $((y % 1000)) -eq 0 ]] && echo "[$y/$bound]"
}

for ((y=0; y<=$bound; y++)); do
    search "$y"
done
