#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")
target=$([[ "$1" == "-e" ]] && echo "10" || echo "2000000")

readarray -t lines < "$infile"
declare -A graph=()
declare -A pairs=()
declare -a ranges=()
declare -a ont=()

lx=0
hx=0
ly=0
hy=0

for n in "${lines[@]}"; do
    [[ "${n%:*}" =~ x=-?[0-9]+ ]] && sx="${BASH_REMATCH[0]#*=}"
    [[ "${n%:*}" =~ y=-?[0-9]+ ]] && sy="${BASH_REMATCH[0]#*=}"
    [[ "${n#*:}" =~ x=-?[0-9]+ ]] && bx="${BASH_REMATCH[0]#*=}"
    [[ "${n#*:}" =~ y=-?[0-9]+ ]] && by="${BASH_REMATCH[0]#*=}"
    pairs["$sx,$sy"]="$bx,$by"
    plots+=("$sx,$sy")
    if [[ $sy -eq $target ]]; then
        ont+=("$sx,$sy")
    fi
    if [[ $by -eq $target ]]; then
        ont+=("$bx,$by")
    fi
    if [[ $sx -gt $hx ]]; then
        hx=$sx
    elif [[ $sx -lt $lx ]]; then
        lx=$sx
    elif [[ $sy -gt $hy ]]; then
        hy=$sy
    elif [[ $sy -lt $ly ]]; then
        ly=$sy
    fi
    if [[ $bx -gt $hx ]]; then
        hx=$bx
    elif [[ $bx -lt $lx ]]; then
        lx=$bx
    elif [[ $by -gt $hy ]]; then
        hy=$by
    elif [[ $by -lt $ly ]]; then
        ly=$by
    fi
done

IFS=$'\n' ont=($(echo "${ont[*]}" | sort | uniq)); unset IFS

for s in "${!pairs[@]}"; do
    b="${pairs[$s]}"
    graph[$s]="S"
    graph[$b]="B"
    sx=${s%,*}
    sy=${s#*,}
    dx=$((sx - ${b%,*}))
    dy=$((sy - ${b#*,}))
    d1=$((${dx#-} + ${dy#-})) ## distance to beacon
    dx2=$((${sx#-} - sx))
    dy2=$((${sy#-} - ${target#-}))
    d2=$((${dx2#-} + ${dy2#-})) ## distance to target
    if [[ ${d2#-} -le ${d1#-} ]]; then
        xr=$((${d1#-} - ${d2#-}))
        xs=$((sx - ${xr#-}))
        xe=$((sx + ${xr#-}))
        ranges+=("$xs,$xe")
    fi
done

IFS=$'\n' ranges=($(echo "${ranges[*]}" | sort -n)); unset IFS

let ll  # last low
let lh  # last high
for ((i=0; i<${#ranges[@]}; i++)); do
    r="${ranges[$i]}"
    rs=${r%,*}
    re=${r#*,}
    if [[ $i -eq 0 ]]; then
        ll=$rs
        lh=$re
        continue
    fi
    if [[ $rs -gt $ll && $re -lt $lh ]]; then
        continue
    elif [[ $rs -gt $ll && $rs -le $lh && $re -gt $lh ]]; then
        lh=$re
    fi
done

echo $((lh - ll - ${#ont[@]} + 1))
