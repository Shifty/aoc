#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"
declare -A chmap=(["S"]=1 ["E"]=26)
declare -A graph
declare -A nodes
declare -A shortest
declare -a queue
start=""
end=""

for c in {a..z}; do
    chmap[$c]=$(($(printf '%d' "'$c") - 96))
done

for ((yi=0; yi<${#lines[@]}; yi++)); do
    line=${lines[$yi]}
    for ((xi=0; xi<${#line}; xi++)); do
        c=${line:$xi:1}
        w=${chmap[$c]}
        if [[ $c == "S" ]]; then
            start="$xi,$yi"
            queue+=($start)
        elif [[ $c == "E" ]]; then
            end="$xi,$yi"
        fi
        nodes["$xi,$yi"]=$w
    done
done

for ((yi=0; yi<${#lines[@]}; yi++)); do
    for ((xi=0; xi<${#lines[0]}; xi++)); do
        cw=${nodes["$xi,$yi"]}
        edges=()
        if [[ $yi -ne 0 ]]; then
            n=${nodes["$xi,$((yi - 1))"]}
            if [[ $((n - cw)) -le 1 ]]; then
                edges+=("$xi,$((yi - 1))")
            fi
        fi
        if [[ $xi -ne $((${#lines[0]} - 1)) ]]; then
            e=${nodes["$((xi + 1)),$yi"]}
            if [[ $((e - cw)) -le 1 ]]; then
                edges+=("$((xi + 1)),$yi")
            fi
        fi
        if [[ $yi -ne $((${#lines[@]} - 1)) ]]; then
            s=${nodes["$xi,$((yi + 1))"]}
            if [[ $((s - cw)) -le 1 ]]; then
                edges+=("$xi,$((yi + 1))")
            fi
        fi
        if [[ $xi -ne 0 ]]; then
            w=${nodes["$((xi - 1)),$yi"]}
            if [[ $((w - cw)) -le 1 ]]; then
                edges+=("$((xi - 1)),$yi")
            fi
        fi
        graph["$xi,$yi"]="${edges[@]}"
    done
done

while [[ ${#queue[@]} -gt 0 ]]; do
    next=${queue[0]}
    IFS=";" path=($next) IFS=" "
    cell=${path[0]}
    queue=(${queue[@]/$next})

    if [[ $cell == $end ]]; then
        IFS=";" steps=${#path[@]} IFS=";"
        ((steps--))
        echo "done in $steps steps"
        break
    fi

    edges=(${graph[$cell]})
    for edge in ${edges[@]}; do
        if ! [[ " $next " =~ " $edge " ]]; then
            IFS=";" len=${#path[@]} IFS=" "
            if ! [[ " ${!shortest[@]} " =~ " $edge " ]] || [[ $len -lt ${shortest[$edge]} ]]; then
                shortest[$edge]=$len
                queue+=("$edge;$next")
            fi
        fi
    done
done
