#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"
rows=()
cols=()

i=1
for line in "${lines[@]}"; do
    rows+=($(echo "$line"))
    cols+=($(cut -c $i "$infile" | tr -d "\n"))
    ((i++))
done

visible=0
for ((x=0; x<${#rows[@]}; x++)); do
    row="${rows[$x]}"
    for ((y=0; y<${#row}; y++)); do
        col="${cols[$y]}"
        tree="${row:$y:1}"

        row_left="${row:0:$y}"
        row_right="${row:$(($y + 1))}"
        col_up="${col:0:$x}"
        col_down="${col:$(($x + 1))}"

        if [[ $x -eq 0 || $y -eq 0 ]]; then
            ((visible++))
        elif [[ $((x + 1)) -eq ${#row} ]]; then
            ((visible++))
        elif [[ $((y + 1)) -eq ${#col} ]]; then
            ((visible++))
        elif ! [[ $row_left =~ [$tree-9] ]]; then
            ((visible++))
        elif ! [[ $row_right =~ [$tree-9] ]]; then
            ((visible++))
        elif ! [[ $col_up =~ [$tree-9] ]]; then
            ((visible++))
        elif ! [[ $col_down =~ [$tree-9] ]]; then
            ((visible++))
        fi
    done
done

echo "$visible"
