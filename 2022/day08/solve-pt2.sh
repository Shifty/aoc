#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"
rows=()
cols=()

i=1
for line in "${lines[@]}"; do
    rows+=($(echo "$line"))
    cols+=($(cut -c $i "$infile" | tr -d "\n"))
    ((i++))
done

highest=0
for ((x=0; x<${#rows[@]}; x++)); do
    row="${rows[$x]}"
    for ((y=0; y<${#row}; y++)); do
        col="${cols[$y]}"
        tree="${row:$y:1}"

        row_left="${row:0:$y}"
        len=${#row_left}
        for ((i=0;i<len;i++)); do
            row_left=${row_left:i*2:1}$row_left;
        done;
        row_left=${row_left:0:len}

        row_right="${row:$(($y + 1))}"

        col_up="${col:0:$x}"
        len=${#col_up}
        for ((i=0;i<len;i++)); do
            col_up=${col_up:i*2:1}$col_up;
        done;
        col_up=${col_up:0:len}

        col_down="${col:$(($x + 1))}"

        s1=0
        if [[ -n $row_left ]]; then
            for ((i=0; i<${#row_left}; i++)); do
                ((s1++))
                if [[ ${row_left:$i:1} -ge $tree ]]; then
                    break
                fi
            done
        fi

        s2=0
        if [[ -n $row_right ]]; then
            for ((i=0; i<${#row_right}; i++)); do
                ((s2++))
                if [[ ${row_right:$i:1} -ge $tree ]]; then
                    break
                fi
            done
        fi

        s3=0
        if [[ -n $col_up ]]; then
            for ((i=0; i<${#col_up}; i++)); do
                ((s3++))
                if [[ ${col_up:$i:1} -ge $tree ]]; then
                    break
                fi
            done
        fi

        s4=0
        if [[ -n $col_down ]]; then
            for ((i=0; i<${#col_down}; i++)); do
                ((s4++))
                if [[ ${col_down:$i:1} -ge $tree ]]; then
                    break
                fi
            done
        fi

        s=$((s1 * s2 * s3 * s4))
        if [[ $s -gt $highest ]]; then
            highest=$s
        fi
    done
done

echo "$highest"
