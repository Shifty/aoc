#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t sacks < "$infile"
chars=$(echo {{a..z},{A..Z}} | sed 's/ //g')

pos () {
    rest="${chars#*$1}"
    echo $((${#chars} - ${#rest} - ${#1} + 1))
}

parts=()
for n in "${sacks[@]}"; do
    offset=$((${#n} / 2))
    a_slug=${n:$offset}
    b_slug=${n:0:$offset}
    parts+=($(echo "$a_slug" | grep -oP "[$b_slug]" | head -1))
done

sum=0
for p in "${parts[@]}"; do
    for n in "${p[@]}"; do
        ((sum += $(pos $n)))
    done
done
echo "$sum"
