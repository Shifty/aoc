#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t sacks < "$infile"
chars=$(echo {{a..z},{A..Z}} | sed 's/ //g')

pos () {
    rest="${chars#*$1}"
    echo $((${#chars} - ${#rest} - ${#1} + 1))
}

parts=()
len=$((${#sacks[@]} / 3))
for n in $(seq 1 $len); do
    a_slug=${sacks[((i++))]}
    b_slug=${sacks[((i++))]}
    c_slug=${sacks[((i++))]}
    parts+=($(echo "$a_slug" | grep -oP "[$b_slug]" | grep -oP "[$c_slug]" | head -1))
done

sum=0
for p in "${parts[@]}"; do
    for n in "${p[@]}"; do
        ((sum += $(pos $n)))
    done
done
echo "$sum"
