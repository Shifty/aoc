#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

parse () {
    cat "$infile" \
    | grep -oP "$1" \
    | sed 's/[ABC]\s//' \
    | sed "s/.*/&+$2/" \
    | tr 'XYZ' '123' \
    | paste -sd+ | bc
}

l=$(parse "(A Z|B X|C Y)" "0")
d=$(parse "(A X|B Y|C Z)" "3")
w=$(parse "(A Y|B Z|C X)" "6")

echo $w+$l+$d | bc
