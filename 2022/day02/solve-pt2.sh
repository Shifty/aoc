#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

parse () {
    echo "$3" \
    | grep -oP "$1" \
    | sed 's/[ABC]\s//' \
    | sed "s/.*/&+$2/" \
    | tr 'XYZ' '123' \
    | paste -sd+ | bc
}

lose () {
    cat "$infile" \
    | grep -oP "$1" \
    | sed 's/A X/A Z/' \
    | sed 's/C X/C Y/' \
    | paste -sd+
}

draw () {
    cat "$infile" \
    | grep -oP "$1" \
    | sed 's/A Y/A X/' \
    | sed 's/C Y/C Z/' \
    | paste -sd+
}

win () {
    cat "$infile" \
    | grep -oP "$1" \
    | sed 's/A Z/A Y/' \
    | sed 's/C Z/C X/' \
    | paste -sd+
}

l_slug=$(lose "(A X|B X|C X)")
d_slug=$(draw "(A Y|B Y|C Y)")
w_slug=$(win "(A Z|B Z|C Z)")

l=$(parse "(A Z|B X|C Y)" "0" "$l_slug")
d=$(parse "(A X|B Y|C Z)" "3" "$d_slug")
w=$(parse "(A Y|B Z|C X)" "6" "$w_slug")

echo $w+$l+$d | bc
