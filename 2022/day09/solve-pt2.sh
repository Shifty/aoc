#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example2.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

knots=()
for i in {0..9}; do
    knots+=("0,0")
done

visited=("0,0")

movehead() {
    local k1="${knots[0]}"
    local head_x="${k1%,*}"
    local head_y="${k1#*,}"
    case "$1" in
        "R") ((head_x++));;
        "L") ((head_x--));;
        "U") ((head_y++));;
        "D") ((head_y--));;
    esac
    knots[0]="${head_x},${head_y}"
}

move() {
    movehead $1
    for ((k=1; k<=9; k++)); do
        local k1="${knots[$((k - 1))]}"
        local head_x="${k1%,*}"
        local head_y="${k1#*,}"
        local k2="${knots[$k]}"
        local tail_x="${k2%,*}"
        local tail_y="${k2#*,}"
        _x=$((head_x - tail_x))
        _y=$((head_y - tail_y))
        if [[ $_x -gt 0 ]]; then sn_x="1"; elif [[ $_x -lt 0 ]]; then sn_x="-1"; else sn_x="0"; fi
        if [[ $_y -gt 0 ]]; then sn_y="1"; elif [[ $_y -lt 0 ]]; then sn_y="-1"; else sn_y="0"; fi
        if [[ ($head_x -ne $tail_x && $head_y -ne $tail_y) && (${_x#-} -gt 1 || ${_y#-} -gt 1) ]]; then
            ((tail_x+=sn_x))
            ((tail_y+=sn_y))
        elif [[ ${_x#-} -gt 1 ]]; then
            ((tail_x+=sn_x))
        elif [[ ${_y#-} -gt 1 ]]; then
            ((tail_y+=sn_y))
        fi
        knots[$((k - 1))]="${head_x},${head_y}"
        knots[$k]="${tail_x},${tail_y}"
    done
    t="${knots[9]}"
    visited+=("${t%,*},${t#*,}")
}

for n in "${lines[@]}"; do
    direction="${n% *}"
    distance="${n#* }"
    for ((i=0; i<$distance; i++)); do
        move $direction
    done
done

printf '%s\n' "${visited[@]}" | sort -n | uniq | wc -l
