#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"
head_x=0
head_y=0
tail_x=0
tail_y=0
visited=("0,0")

move() {
    case "$1" in
        "R") ((head_x++));;
        "L") ((head_x--));;
        "U") ((head_y++));;
        "D") ((head_y--));;
    esac
    _x=$((head_x - tail_x))
    _y=$((head_y - tail_y))
    if [[ $_x -gt 0 ]]; then sn_x="1"; elif [[ $_x -lt 0 ]]; then sn_x="-1"; else sn_x="0"; fi
    if [[ $_y -gt 0 ]]; then sn_y="1"; elif [[ $_y -lt 0 ]]; then sn_y="-1"; else sn_y="0"; fi
    if [[ ($head_x -ne $tail_x && $head_y -ne $tail_y) && (${_x#-} -gt 1 || ${_y#-} -gt 1) ]]; then
        ((tail_x+=sn_x))
        ((tail_y+=sn_y))
    elif [[ ${_x#-} -gt 1 ]]; then
        ((tail_x+=sn_x))
    elif [[ ${_y#-} -gt 1 ]]; then
        ((tail_y+=sn_y))
    fi
    visited+=("${tail_x},${tail_y}")
}

for n in "${lines[@]}"; do
    direction="${n% *}"
    distance="${n#* }"
    for ((i=0; i<$distance; i++)); do
        move $direction
    done
done

printf '%s\n' "${visited[@]}" | sort -n | uniq | wc -l
