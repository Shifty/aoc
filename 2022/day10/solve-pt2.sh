#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

x=1
cycles=0
rowindex=0

declare -A rows=(
    [0]=""
    [1]=""
    [2]=""
    [3]=""
    [4]=""
    [5]=""
)

i=0
tick() {
    rowindex=$((cycles / 40))
}

drawpixel() {
    local row="${rows[$rowindex]}"
    cur=$((cycles - 1))
    while [[ $cur -ge 40 ]]; do
        ((cur-=40))
    done
    if [[ $cur -ge $((x - 1)) && $cur -le $((x + 1)) ]]; then
        row+="#"
    else
        row+="."
    fi
    rows[$rowindex]=$row
}

addx() {
    ((cycles++))
    drawpixel
    tick
    ((cycles++))
    drawpixel
    tick
    ((x+=$1))
}

for n in "${lines[@]}"; do
    case "$n" in
        addx*) addx ${n#* };;
        noop) ((cycles++)) && drawpixel && tick;;
    esac
done

for i in "${rows[@]}"; do
    echo "$i"
done
