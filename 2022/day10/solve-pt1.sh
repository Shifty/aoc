#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

x=1
cycles=0

declare -A milestones=(
    [20]=0
    [60]=0
    [100]=0
    [140]=0
    [180]=0
    [220]=0
)

signalcheck() {
    if [[ $cycles -eq 20 || $(((cycles - 20) % 40)) -eq 0 ]]; then
        milestones[$cycles]=$((x * cycles))
    fi
}

addx() {
    ((cycles++))
    signalcheck
    ((cycles++))
    signalcheck
    ((x+=$1))
}

for n in "${lines[@]}"; do
    case "$n" in
        addx*) addx ${n#* };;
        noop) ((cycles++)) && signalcheck;;
    esac
done

for i in "${milestones[@]}"; do
    echo $i
done | paste -sd+ | bc
