#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

declare -a items=()
declare -a ops=()
declare -a tests=()
declare -a passes=()
declare -a fails=()
declare -a inspects=()

throw() {
    local i=(${items[$1]})
    local o=${ops[$1]}
    local t=${tests[$1]}
    local p=${passes[$1]}
    local f=${fails[$1]}
    for item in "${i[@]}"; do
        if [[ ${#o} -eq 1 ]]; then
            local wl=$(((item $o item) / 3))
        else
            local wl=$(((item $o) / 3))
        fi
        if ((wl % t == 0)); then
            local nm=${items[$p]}
            nm+=" $wl"
            items[$p]="${nm# *}"
        else
            local nm=${items[$f]}
            nm+=" $wl"
            items[$f]="${nm# *}"
        fi
        local ins=${inspects[$1]}
        inspects[$1]=$((++ins))
    done
    items[$1]=""
}

i=0
curm=0
stress=0
while (( $i < ${#lines[@]} )); do
    if [[ -z "${lines[$i]}" ]]; then ((i++)) && continue; fi

    m=$(echo "${lines[$i]}" | grep -oP "(?<=Monkey )\d+")
    ((i++))

    it=$(echo "${lines[$i]}" | grep -oP "(?<=Starting items: )(\d+|, )+" | tr -d ",")
    items[$curm]="$it"
    ((i++))

    o=$(echo "${lines[$i]}" | grep -oP "(?<=Operation: new = old ).+")
    if [[ "${o#* }" == "old" ]]; then o="${o% *}"; fi
    ops[$curm]="$o"
    ((i++))

    t=$(echo "${lines[$i]}" | grep -oP "(?<=Test: divisible by )\d+")
    tests[$curm]="$t"
    ((i++))

    p=$(echo "${lines[$i]}" | grep -oP "(?<=If true: throw to monkey )\d+")
    passes[$curm]="$p"
    ((i++))

    f=$(echo "${lines[$i]}" | grep -oP "(?<=If false: throw to monkey )\d+")
    fails[$curm]="$f"
    ((i++))
    ((curm++))
done

for ((x=0; x<20; x++)); do
    for ((m=0; m<${#items[@]}; m++)); do
        throw $m
    done
done

echo ${inspects[@]} | grep -oP "\d+" | sort -n | tail -2 | paste -sd* | bc
