#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"
ordered=0

makelist() {
    if [[ "$1" =~ ^\[.*\]$ ]]; then
        echo "$1"
    else
        echo "[$1]"
    fi
}

islist() {
    if [[ "$1" =~ ^\[.*\]$ ]]; then
        echo 1
    else
        echo 0
    fi
}

check() {
    if [[ $1 -lt $2 ]]; then
        echo 0
    elif [[ $1 -gt $2 ]]; then
        echo 1
    fi
}

compare() {
    local -a left="($(echo "$1" | jq -cr .[]))"
    local -a right="($(echo "$2" | jq -cr .[]))"
    for ((i=0; i==i; i++)); do
        local lv="${left[$i]}"
        local rv="${right[$i]}"
        if [[ -z "$lv" && -z "$rv" ]]; then
            return
        elif [[ -z "$lv" ]]; then
            res=0
        elif [[ -z "$rv" ]]; then
            res=1
        else
            local lres=$(islist "$lv")
            local rres=$(islist "$rv")
            if [[ $((lres + rres)) -eq 2 ]]; then
                res=$(compare "$lv" "$rv")
            elif [[ $((lres + rres)) -eq 1 ]]; then
                lv=$(makelist "$lv")
                rv=$(makelist "$rv")
                res=$(compare "$lv" "$rv")
            else
                res=$(check "$lv" "$rv")
            fi
        fi
        if [[ -n $res ]]; then
            echo $res
            return
        fi
    done
}


index=1
for ((i=0; i==i; i+=3)); do
    if [[ $i -gt ${#lines[@]} ]]; then
        break
    fi
    left=${lines[$i]}
    right=${lines[$((i + 1))]}
    res=$(compare "$left" "$right")
    if [[ $res -eq 0 ]]; then
        ((ordered+=index))
    fi
    ((index++))
done

echo $ordered
