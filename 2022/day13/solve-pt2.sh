#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"

makelist() {
    if [[ "$1" =~ ^\[.*\]$ ]]; then
        echo "$1"
    else
        echo "[$1]"
    fi
}

islist() {
    if [[ "$1" =~ ^\[.*\]$ ]]; then
        echo 0
    else
        echo 1
    fi
}

check() {
    if [[ $1 -lt $2 ]]; then
        echo 0
    elif [[ $1 -gt $2 ]]; then
        echo 1
    fi
}

compare() {
    local -a left="($(echo "$1" | jq -cr .[]))"
    local -a right="($(echo "$2" | jq -cr .[]))"
    for ((i=0; i==i; i++)); do
        local lv="${left[$i]}"
        local rv="${right[$i]}"
        if [[ -z "$lv" && -z "$rv" ]]; then
            return
        elif [[ -z "$lv" ]]; then
            res=0
        elif [[ -z "$rv" ]]; then
            res=1
        else
            local lres=$(islist "$lv")
            local rres=$(islist "$rv")
            if [[ $((lres + rres)) -eq 0 ]]; then
                res=$(compare "$lv" "$rv")
            elif [[ $((lres + rres)) -eq 1 ]]; then
                lv=$(makelist "$lv")
                rv=$(makelist "$rv")
                res=$(compare "$lv" "$rv")
            else
                res=$(check "$lv" "$rv")
            fi
        fi
        if [[ -n $res ]]; then
            echo $res
            return
        fi
    done
}

lines+=("[[2]]" "[[6]]")
xi=0
yi=0

for ((i=0; i<${#lines[@]}; i++)); do
    n="${lines[$i]}"
    if [[ -z "$n" ]]; then
        continue
    fi
    res=$(compare "$n" "[[2]]")
    if [[ $res -eq 0 ]]; then
        ((xi++))
    fi
    res=$(compare "$n" "[[6]]")
    if [[ $res -eq 0 ]]; then
        ((yi++))
    fi
done

echo $((xi * yi))
