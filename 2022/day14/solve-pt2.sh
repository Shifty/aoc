#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"
declare -A graph

lx=500
ly=0
hx=500
hy=0

makewall() {
    path=($(echo "${1// ->/}"))
    for p in "${path[@]}"; do
        x="${p%,*}"
        y="${p#*,}"
        graph["$x,$y"]="#"
        if [[ -n $lastx ]]; then
            if [[ $lastx -lt $x ]]; then
                for i in $(eval echo {$lastx..$x}); do
                    graph["$i,$y"]="#"
                done
            elif [[ $lastx -gt $x ]]; then
                for i in $(eval echo {$x..$lastx}); do
                    graph["$i,$y"]="#"
                done
            elif [[ $lasty -lt $y ]]; then
                for i in $(eval echo {$lasty..$y}); do
                    graph["$x,$i"]="#"
                done
            elif [[ $lasty -gt $y ]]; then
                for i in $(eval echo {$y..$lasty}); do
                    graph["$x,$i"]="#"
                done
            fi
        fi
        if [[ $x -gt $hx ]]; then
            hx=$x
        elif [[ $x -lt $lx ]]; then
            lx=$x
        fi
        if [[ $y -gt $hy ]]; then
            hy=$y
        elif [[ $y -lt $ly ]]; then
            ly=$y
        fi
        local lastx=$x
        local lasty=$y
    done
}

for n in "${lines[@]}"; do
    makewall "$n"
done

((hy+=2))

i=0
checkdone() {
    if [[ "$1,$2" == "500,0" ]]; then
        ((d1++)); ((d2++))
    fi
}

getpath() {
    local x=500
    local y=0
    for ((d2=0; d2<1; d2)); do
        if [[ $((y + 1)) -eq $hy ]]; then
            graph["$x,$y"]="o"
            ((i++)); ((d2++))
        elif [[ ${graph["$x,$((y + 1))"]} == "" ]]; then
            ((y++))
        elif [[ ${graph["$((x - 1)),$((y + 1))"]} == "" ]]; then
            ((x--)); ((y++))
        elif [[ ${graph["$((x + 1)),$((y + 1))"]} == "" ]]; then
            ((x++)); ((y++))
        else
            graph["$x,$y"]="o"
            ((i++)); ((d2++))
        fi
        checkdone $x $y
    done
}

for ((d1=0; d1<1; d1)); do
    getpath
done

echo $i
