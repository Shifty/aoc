SCRIPT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

HELP_TEXT="Advent of Code - Quick Setup

Usage: ./new.sh -y 2022 -d 1

Options:
    -h, --help           Displays this message and exits.
    -y, --year           The year of AoC to create.
    -d, --day            The day of AoC to create."

[[ $# -eq 0 ]] && echo "$HELP_TEXT" && exit 0
[[ "$1" =~ ^(-h|help|--help)$ ]] && echo "$HELP_TEXT" && exit 0

while (( "$#" )); do
    case "$1" in
        -y|--year) YEAR=$2; shift 2;;
        -d|--day) DAY=$2; shift 2;;
        *) echo "fatal: unknown argument '$1'"; exit 1;;
    esac
done

[[ -z "$YEAR" ]] && echo "fatal: 'year' is required" && exit 1
[[ -z "$DAY" ]] && echo "fatal: 'day' is required" && exit 1
DAY=$(printf '%02d' $DAY)

DAY_PATH="${SCRIPT_PATH}/${YEAR}/day${DAY}/"
[[ -d "$DAY_PATH" ]] && echo "fatal: path already exists" && exit 1

mkdir "$DAY_PATH"
cd "$DAY_PATH"

FILE_BASE='#!/usr/bin/env bash

infile=$([[ "$1" == "-e" ]] && echo "example.txt" || echo "puzzle.txt")

readarray -t lines < "$infile"'

touch example.txt puzzle.txt
echo "$FILE_BASE" > solve-pt1.sh
echo "$FILE_BASE" > solve-pt2.sh

echo "day $(printf '%01d' $DAY) created"
